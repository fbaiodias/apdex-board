import Component from './component'
import Host from './host'

class Hosts extends Component {
  render () {
    const {apdexBoard, showAsList} = this.props
    const hostsNames = apdexBoard.getHostNames()

    const classes = ['hosts']
    if (!showAsList) {
      classes.push('grid')
    }

    return `
      <div class="${classes.join(' ')}">
        ${hostsNames.map(
          hostName => new Host({hostName, apdexBoard}).render()
        ).join('')}
      </div>
    `
  }
}

export default Hosts