import Component from './component'

class App extends Component {
  render () {
    const {app} = this.props

    return `
      <div class="host__app" data-version="${app.getVersion()}">
        <div class="host__app__apdex">${app.getApdex()}</div>
        <div class="host__app__name">${app.getName()}</div>
      </div>
    `
  }
}

export default App