import Component from './component'
import App from './app'

class Host extends Component {
  render () {
    const {hostName, apdexBoard} = this.props

    return `
      <div class="host">
        <h3 class="host__name">${hostName}</h3>
        ${apdexBoard.getTopAppsByHost(hostName, 5).map(
          app => new App({app}).render()
        ).join('')}
      </div>
    `
  }
}

export default Host