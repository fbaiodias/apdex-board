import Component from './component'
import Hosts from './hosts'

class Main extends Component {
  render () {
    const {apdexBoard, showAsList} = this.props

    return `
      <div class="header">
        <h1 class="header__title">Apps by Host</h1>
        <div class="header__user">for user francisco@baiodias.com</div>
        <div class="header__checkbox">
          <input id="showAsList" type="checkbox" ${showAsList ? 'checked' : ''}>
          <label for="showAsList">${showAsList ? 'Show an awesome grid': 'Show as list'}</label>
        </div>
      </div>
      ${new Hosts({apdexBoard, showAsList}).render()}
    `
  }
}

export default Main