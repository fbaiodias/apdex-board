import ApdexBoard from './models/apdex-board'
import App from './models/app'
import { getHostAppData } from './transport'
import Main from './components/main'

const apdexBoard = (window.apdexBoard = new ApdexBoard())

const render = showAsList => {
  const root = document.getElementById('root')
  root.innerHTML = new Main({ apdexBoard, showAsList }).render()

  const showAsListEl = document.getElementById('showAsList')
  showAsListEl.onchange = e => {
    render(e.target.checked)
  }

  const apps = document.querySelectorAll('.host__app')
  apps.forEach(app => {
    app.onclick = e => {
      alert(`Verison: ${e.currentTarget.dataset.version}`)
    }
  })
}

getHostAppData().then(hostAppData => {
  hostAppData.forEach(appData => {
    const app = new App(appData)
    apdexBoard.addAppToHosts(app)
  })

  render()
})
