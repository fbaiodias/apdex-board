import Host from './host'

class ApdexBoard {
  constructor () {
    this.hosts = {}
  }

  getHostNames () {
    return Object.keys(this.hosts)
  }

  getTopAppsByHost (hostName, num) {
    return this.hosts[hostName].getTopApps(num)
  }

  addAppToHosts (app) {
    app.getHosts().forEach(hostName => {
      if (!this.hosts[hostName]) {
        this.hosts[hostName] = new Host(hostName)
      }

      this.hosts[hostName].addApp(app)
    })
  }

  removeAppFromHosts (app) {
    app.getHosts().forEach(hostName => {
      if (this.hosts[hostName]) {
        this.hosts[hostName].removeApp(app)
      }
    })
  }
}

export default ApdexBoard
