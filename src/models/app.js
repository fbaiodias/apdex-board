class App {
  constructor ({ name, apdex, version, host }) {
    this.name = name
    this.apdex = apdex
    this.version = version
    this.hosts = host
  }

  getName () {
    return this.name
  }

  getApdex () {
    return this.apdex
  }

  getVersion () {
    return this.version
  }

  getHosts () {
    return this.hosts
  }
}

export default App
