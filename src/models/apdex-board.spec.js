import App from './app'
import ApdexBoard from './apdex-board'

describe('Models: ApdexBoard', () => {
  const hostNameA = 'abc.def.com'
  const hostNameB = 'ghi.jkl.com'

  const appA = new App({
    name: 'App A',
    apdex: 50,
    host: [hostNameA, hostNameB]
  })
  const appB = new App({
    name: 'App B',
    apdex: 70,
    host: [hostNameA]
  })
  const appC = new App({
    name: 'App C',
    apdex: 60,
    host: [hostNameA, hostNameB]
  })
  const appD = new App({
    name: 'App D',
    apdex: 40,
    host: [hostNameA]
  })
  const appE = new App({
    name: 'App E',
    apdex: 90,
    host: [hostNameA, hostNameB]
  })
  const appF = new App({
    name: 'App F',
    apdex: 80,
    host: [hostNameA]
  })

  it('apdexBoard.getHostNames() returns hostNames', () => {
    const apdexBoard = new ApdexBoard()

    apdexBoard.addAppToHosts(appA)

    expect(apdexBoard.getHostNames()).toEqual([hostNameA, hostNameB])
  })

  it('apdexBoard.addAppToHosts() adds app to both hosts', () => {
    const apdexBoard = new ApdexBoard()

    apdexBoard.addAppToHosts(appA)
    apdexBoard.addAppToHosts(appB)
    apdexBoard.addAppToHosts(appC)
    apdexBoard.addAppToHosts(appD)
    apdexBoard.addAppToHosts(appE)
    apdexBoard.addAppToHosts(appF)

    expect(apdexBoard.hosts[hostNameA].apps).toEqual([
      appE,
      appF,
      appB,
      appC,
      appA,
      appD
    ])
    expect(apdexBoard.hosts[hostNameB].apps).toEqual([appE, appC, appA])
  })

  it('apdexBoard.getTopAppsByHost() get the tops N apps for the host', () => {
    const apdexBoard = new ApdexBoard()

    apdexBoard.addAppToHosts(appA)
    apdexBoard.addAppToHosts(appB)
    apdexBoard.addAppToHosts(appC)
    apdexBoard.addAppToHosts(appD)
    apdexBoard.addAppToHosts(appE)
    apdexBoard.addAppToHosts(appF)

    expect(apdexBoard.getTopAppsByHost(hostNameA, 3)).toEqual([
      appE,
      appF,
      appB
    ])
  })

  it('apdexBoard.removeAppFromHosts() removes app from both hosts', () => {
    const apdexBoard = new ApdexBoard()

    apdexBoard.addAppToHosts(appA)
    apdexBoard.addAppToHosts(appB)
    apdexBoard.addAppToHosts(appC)
    apdexBoard.addAppToHosts(appD)
    apdexBoard.addAppToHosts(appE)
    apdexBoard.addAppToHosts(appF)

    apdexBoard.removeAppFromHosts(appE)

    expect(apdexBoard.hosts[hostNameA].apps).toEqual([
      appF,
      appB,
      appC,
      appA,
      appD
    ])
    expect(apdexBoard.hosts[hostNameB].apps).toEqual([appC, appA])
  })
})
