import App from './app'

describe('Models: App', () => {
  const appData = {
    name: 'Small Fresh Pants - Kautzer - Boyer, and Sons',
    contributors: [
      'Edwin Reinger',
      'Ofelia Dickens',
      'Hilbert Cole',
      'Helen Kuphal',
      'Maurine McDermott Sr.'
    ],
    version: 7,
    apdex: 68,
    host: [
      '7e6272f7-098e.dakota.biz',
      '9a450527-cdd9.kareem.info',
      'e7bf58af-f0be.dallas.biz'
    ]
  }

  const app = new App(appData)

  it('app.getName() returns the name', () => {
    expect(app.getName()).toEqual(appData.name)
  })
  it('app.getApdex() returns the apdex', () => {
    expect(app.getApdex()).toEqual(appData.apdex)
  })
  it('app.getVersion() returns the version', () => {
    expect(app.getVersion()).toEqual(appData.version)
  })
})
