function binaryInsertByApdex (app, array, startVal, endVal) {
  let length = array.length
  let start = typeof startVal !== 'undefined' ? startVal : 0
  let end = typeof endVal !== 'undefined' ? endVal : length - 1
  let m = start + Math.floor((end - start) / 2)

  if (length === 0) {
    array.push(app)
    return
  }

  if (app.getApdex() <= array[end].getApdex()) {
    array.splice(end + 1, 0, app)
    return
  }

  if (app.getApdex() >= array[start].getApdex()) {
    array.splice(start, 0, app)
    return
  }

  if (start >= end) {
    return
  }

  if (app.getApdex() >= array[m].getApdex()) {
    binaryInsertByApdex(app, array, start, m - 1)
    return
  }

  if (app.getApdex() <= array[m].getApdex()) {
    binaryInsertByApdex(app, array, m + 1, end)
  }
}

class Host {
  constructor (name) {
    this.name = name
    this.apps = []
  }

  addApp (app) {
    binaryInsertByApdex(app, this.apps)
  }

  removeApp (app) {
    this.apps = this.apps.filter(a => a.name !== app.name)
  }

  getName () {
    return this.name
  }

  getTopApps (num = 25) {
    return this.apps.slice(0, num)
  }
}

export default Host
