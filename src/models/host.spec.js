import App from './app'
import Host from './host'

describe('Models: Host', () => {
  const hostName = 'abc.def.com'

  const appA = new App({
    name: 'App A',
    apdex: 50,
    host: [hostName]
  })
  const appB = new App({
    name: 'App B',
    apdex: 70,
    host: [hostName]
  })
  const appC = new App({
    name: 'App C',
    apdex: 60,
    host: [hostName]
  })
  const appD = new App({
    name: 'App D',
    apdex: 40,
    host: [hostName]
  })
  const appE = new App({
    name: 'App E',
    apdex: 90,
    host: [hostName]
  })
  const appF = new App({
    name: 'App F',
    apdex: 80,
    host: [hostName]
  })

  it('host.getName() returns the name', () => {
    const host = new Host(hostName)
    expect(host.getName()).toEqual(hostName)
  })

  it('host.addApp() when array is empty', () => {
    const host = new Host(hostName)

    host.addApp(appA)

    expect(host.apps).toEqual([appA])
  })

  it('host.addApp() keeps array orderered by apdex', () => {
    const host = new Host(hostName)

    host.addApp(appA)
    host.addApp(appB)
    host.addApp(appC)
    host.addApp(appD)
    host.addApp(appE)
    host.addApp(appF)

    expect(host.apps).toEqual([appE, appF, appB, appC, appA, appD])
  })

  it('host.removeApp() removes the right app', () => {
    const host = new Host(hostName)

    host.addApp(appA)
    host.addApp(appB)
    host.addApp(appC)

    expect(host.apps).toEqual([appB, appC, appA])

    host.removeApp(appB)

    expect(host.apps).toEqual([appC, appA])
  })

  it('host.getTopApps() returns first n apps orderered by apdex', () => {
    const host = new Host(hostName)

    host.addApp(appA)
    host.addApp(appB)
    host.addApp(appC)
    host.addApp(appD)
    host.addApp(appE)
    host.addApp(appF)

    expect(host.getTopApps(3)).toEqual([appE, appF, appB])
  })
})
