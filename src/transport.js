export const getHostAppData = () =>
  window.fetch('host-app-data.json').then(res => res.json())
