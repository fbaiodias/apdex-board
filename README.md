# apdex-board

[http://fbaiodias.gitlab.io/apdex-board/](http://fbaiodias.gitlab.io/apdex-board/)

## usage

`yarn build` builds the application to `public/bundle.js`, along with a sourcemap file for debugging.

`yarn start` launches a server, using [serve](https://github.com/zeit/serve). Navigate to [localhost:5000](http://localhost:5000).

`yarn watch` will continually rebuild the application as your source files change.

`yarn dev` will run `yarn start` and `yarn watch` in parallel.

`yarn test` will run unit tests. Use `yarn test --watch` for development.
